import { NgModule } from '@angular/core'
import { ReactiveFormsModule } from '@angular/forms'

import { LoginComponent } from './pages/login/login.component'
import { AuthRoutingModule } from './auth-routing.module'
import { SharedModule } from '../shared/shared.module'

@NgModule({
  declarations: [
    LoginComponent,
  ],
  imports: [
    AuthRoutingModule,
    SharedModule,
    ReactiveFormsModule,
  ],
  providers: [],
})
export class AuthModule {
}
