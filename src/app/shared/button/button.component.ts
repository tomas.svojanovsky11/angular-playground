import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core'

export enum ButtonType {
  BUTTON = 'button',
  SUBMIT = 'submit',
  RESET = 'reset',
}

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ButtonComponent {

  @Input() type = ButtonType.BUTTON

  @Input() label = ''

  @Input() disabled = false

  @Output() outClick = new EventEmitter<void>()

  onClick () {
    this.outClick.emit()
  }

}
