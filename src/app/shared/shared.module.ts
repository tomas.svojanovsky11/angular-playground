import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RouterModule } from '@angular/router'

import { ButtonComponent } from './button/button.component'
import { InputComponent } from './input/input.component'
import { AuthorizedLayoutComponent } from './layouts/authorized-layout/authorized-layout.component'
import { UnauthorizedLayoutComponent } from './layouts/unauthorized-layout/unauthorized-layout.component'

@NgModule({
  declarations: [
    ButtonComponent,
    InputComponent,
    AuthorizedLayoutComponent,
    UnauthorizedLayoutComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
  ],
  providers: [],
  exports: [
    ButtonComponent,
    InputComponent,
  ]
})
export class SharedModule {
}
