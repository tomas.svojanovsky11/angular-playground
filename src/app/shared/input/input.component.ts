import { ChangeDetectionStrategy, Component, EventEmitter, forwardRef, Input, Output } from '@angular/core'
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms'

// export enum InputType {
//   TEXT = 'text',
//   PASSWORD = 'password',
//   EMAIL = 'email',
// }

export type InputType = 'text' | 'password' | 'email'

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => InputComponent),
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InputComponent implements ControlValueAccessor {

  @Input() type: InputType = 'text'

  @Input() label = ''

  @Input() placeholder = ''

  @Output() outText = new EventEmitter<string>()

  @Input() set value (value: string) {
    this._value = value
    this.onChange(value)
    this.onTouched()
  }

  _value = ''

  onChange: any = () => {
  }
  onTouched: any = () => {
  }

  writeValue (value: string): void {
    this._value = value || ''
  }

  registerOnChange (fn: any) {
    this.onChange = fn
  }

  registerOnTouched (fn: any) {
    this.onTouched = fn
  }

  onChangeValue ($event: any) {
    this.onChange($event.target.value)
  }

}
